class NotAdmin() : User() {

    private val userName = "usernameNotAdmin"
    private val password = "passwordNotAdmin"
    private val permission = true

    override fun add(){
        println("add")
    }

    override fun delete(){
        println("You don't have the permission to do that!")
    }

    override fun getUserName() {
        println(userName)
    }

    override fun getPassword() {
        println(password)
    }

    override fun view(){
        println("view")
    }

    override fun update(){
        println("update")
    }

//    override fun getUserName(){
//        println(this.userName)
//    }
//
//    override fun getPassword(){
//        println(this.password)
//    }
}